# Дефолтный конфигурационный файл Nginx

Дефолтный конфигурационный файл Nginx появляется после его установки.

## Начиная

Эти инструкции предоставят вам способ установки Nginx на Centos 7

### Предпосылки

Что нужно для установки:
1. Хост с ОС Centos 7
2. Доступ в интернет
3. Нужный репозиторий

### Установка

Шаг первый - добавьте репозиторий Nginx

```
sudo yum install epel-release
```

Шаг второй - установите Nginx

```
sudo yum install nginx
```

## Дефолтный конфигурационный файл

Основной файл конфигурации Nginx находится по адресу: 

```
/etc/nginx/nginx.conf
```

### Запуск Nginx

Nginx не запускается сам по себе. Чтобы запустить Nginx, введите:

```
sudo systemctl start nginx
```
#### Настройка Firewall
Если вы используете брандмауэр, выполните следующие команды для разрешения трафика HTTP и HTTPS:

```
sudo firewall-cmd --permanent --zone=public --add-service=http
sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd --reload
```

### Включение автозапуска

Включить Nginx при загрузке системы. Для этого введите следующую команду:

```
sudo systemctl enable nginx
```

## Проверка запуска

Вы можете сразу же сделать выборочную проверку, чтобы убедиться, что все прошло как запланировано, посетив общедоступный IP-адрес вашего сервера в веб-браузере:

```
http://server_domain_name_or_IP/
```

Вы увидите веб-страницу CentOS 7 Nginx по умолчанию, предназначенную для ознакомления и тестирования. Это должно выглядеть примерно так:

![Sart page nginx png]
(./images/WelcomeNginx.png)

## Использованный материал

* [Mastering Markdown](https://guides.github.com/features/mastering-markdown/)
* [README-Template.md](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
* [Markdown online](https://daringfireball.net/projects/markdown/dingus)
## Содействие

* Так как у меня Windows, не стал устанавливать Nginx, просто скопировал конфигурационный файл с этой статьи:
[Введение в NGINX: как его установить и настроить](https://proglib.io/p/nginx/ "Введение в NGINX: как его установить и настроить")

* Текст для составление был взять с статьи: 
[How To Install Nginx on CentOS 7](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-centos-7)

## Авторы

* **Сагынов Байсак** - *Модуль ГИТ ДЗ №4* - [Baisak in Github](https://github.com/BaisakKG)

Смотрите также кто [заставил](https://rebrainme.com/devops/), меня это сделать.

